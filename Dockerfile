# Use the official Node.js image from Docker Hub
FROM node:lts-stretch-slim

# Create user
RUN echo 'create user app'
RUN adduser -disabled-password --gecos '' node_user

#Define user app
RUN echo 'Define user app'
USER node_user

#Define env variables
ENV NPM_CONFIG_PREFIX=~/.npm-global
ENV PATH=$PATH:~/.npm-global/bin
ENV NODE_PATH=~/node_modules;%NODE_PATH%

#Define work directory
RUN echo 'define work directory'
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
RUN echo 'copy files'
COPY package*.json ./

# Install dependencies
RUN echo 'Install npm'
RUN npm install --loglevel=warn;
RUN npm install express

# Copy the rest of your application code
RUN echo 'copy files and permissions' 
COPY . .
COPY --chown=node_user:node_user . .

# Expose the port your app runs on
RUN echo 'Expose port 3000'
EXPOSE 3000

# Command to run your application
CMD node index.js